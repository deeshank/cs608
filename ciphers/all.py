########## CAESAR CIPHER ##########################################
def c_encrypt(n, plaintext):
    from pycipher import caesar
    if n and isinstance(n, int):
        encrypted_txt = caesar.Caesar(n).encipher(plaintext)
        return encrypted_txt
    return None


def c_decrypt(n, encrypted_txt):
    from pycipher import caesar
    if n and isinstance(n, int):
        plaintxt = caesar.Caesar(n).decipher(encrypted_txt)
        return plaintxt
    return None


##################################################################

############## COLUMNAR TRANSPOSITION ############################
def ct_encrypt(key, plaintext):
    from pycipher import ColTrans
    if key and isinstance(key, str):
        encrypted_txt = ColTrans(key).encipher(plaintext)
        plaintext = plaintext.replace(' ', '')
        l = len(encrypted_txt) / len(key)
        txt = ''
        for x in range(len(encrypted_txt)):
            if x % l == 0 and x != 0:
                txt += ' '
            txt += encrypted_txt[x]
        return txt
    return None


def ct_decrypt(key, encrypted_txt):
    from pycipher import ColTrans
    if key and isinstance(key, str):
        plaintxt = ColTrans(key).decipher(encrypted_txt)
        return plaintxt
    return None


##################################################################


def pf_encrypt(key,plaintext):
    from pycipher import playfair
    if key and isinstance(key,str):
        encrypted_txt = playfair.Playfair(key=key).encipher(plaintext)
        return encrypted_txt
    return None

def pf_decrypt(key,ciphertext):
    from pycipher import playfair
    if key and isinstance(key,str):
        plaintext = playfair.Playfair(key=key).decipher(ciphertext)
        return plaintext
    return None


#######################################################################


def p_decrypt(cipher, ciphertext):
    return p_encrypt(inverse_key(cipher), ciphertext)

def p_encrypt(cipher, plaintext):
    plaintext = "".join(plaintext.split(" ")).upper()
    ciphertext = ""
    for pad in range(0, len(plaintext)%len(cipher)*-1%len(cipher)):
        plaintext += "X"
    for offset in range(0, len(plaintext), len(cipher)):
        for element in [a-1 for a in cipher]:
            ciphertext += plaintext[offset+element]
        ciphertext += " "
    return ciphertext[:-1]

def inverse_key(cipher):
    inverse = []
    for position in range(min(cipher),max(cipher)+1,1):
        inverse.append(cipher.index(position)+1)
    print inverse
    return inverse
#
# print p_encrypt([2,4,1,5,3],'loremipsumdolorsitametconsectetueradipiscingelit')
#
# print inverse_key([2,4,1,5,3])
##################################################################################


#
# print pf_encrypt('abcdefghjklmnopqrstuvwxyz','hello one and all')
# print pf_decrypt('abcdefghjklmnopqrstuvwxyz','KCNVNYPCCLEBNV')
#print ct_encrypt('zebras', 'we are discovered flee at once qkjeu')
#print ct_decrypt('zebras', 'EVLNE ACDTK ESEAQ ROFOJ DEECU WIREE')

#print ct_encrypt('method', 'THIS IS A GRADUATE COURSE IN COMPUTER SCIENCE')

# t = c_encrypt(23,'Science may set limits to knowledge but should not set limits to imagination')
# print t
#
#
# print pf_encrypt('subdermatoglyphicfknqvwxz','PZFBKZBJXVPBQIFJFQPQLHKLTIBADBYRQPELRIAKLQPBQIFJFQPQLFJXDFKXQFLK')
#
# print p_encrypt([14,9,3,10,11,15,12,7,5,6,13,1,2,8,4],'HXWANXSFZWYDSQKCIWGXYGCPRKAYEDGAXGUHGQTFGVYDSQKCIWGXYCKQBKXDWIPC')
#
# print p_decrypt([14,9,3,10,11,15,12,7,5,6,13,1,2,8,4],'QZWWYKDSNXSHXFAEPWRKDAGXYYCICGDTXFGSVGUHYGAQGXCCKQDBXWGKQKYIXXPXXXXXXXXWIXC')
#
#
# print pf_decrypt('subdermatoglyphicfknqvwxz','HXWANXSFZWYDSQKCIWGXYGCPRKAYEDGAXGUHGQTFGVYDSQKCIWGXYCKQBKXDWIPC')
#
# print c_decrypt(23,'PZFBKZBJXVPBQIFJFQPQLHKLTIBADBYRQPELRIAKLQPBQIFJFQPQLFJXDFKXQFLK')


for x in range(1,28):
    print c_decrypt(x,'ppaicteskw')