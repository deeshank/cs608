import ModularInverse
from utils import sqnm, find_inverse

R=();

def doubleP(P,a,p):
	R=()
	BN=(3*(P[0]**2))+a;
	BD=2*P[1];
	XN=(BN**2);
	XD=BD**2;
	XN1= ModularInverse.findModularInverse(p, XD);
	XN=((XN*XN1)-2*P[0])%p;
	R=(XN,0);
	YN1=BN*(P[0]-R[0]);
	YN= ModularInverse.findModularInverse(p, BD);
	YN=((YN*YN1)-P[1])%p;
	R=(R[0],YN);
	return R;

def find2P(P,a,p):
	x,y = P
	d = find_inverse(2*y,p,debug=False)
	alpha = sqnm(((3*((x)**2)) + a)*d,1,p,debug=False)

	dSq = find_inverse((2*y)**2,p,debug=False)
	alphaSq = sqnm(((3*((x)**2)) + a)**2*dSq,1,p,debug=False)

	X2p = (alphaSq - 2*x)%p
	Y2p = (alpha*(x-X2p)-y)%p
	return X2p,Y2p


def findxP(x,P,a,p):
	Q = find2P(P,a,p)

	for i in range(x):
		Q = addPQ(P,Q,a,p)
		print Q



def addPQ(P,Q,a,p):
	x1,y1 = P
	x2,y2 = Q

	try:
		chk = sqnm((x1-x2),1,p,debug=False)
	except Exception,e:
		return 0
	if chk == 0:
		return 0
	d = find_inverse(chk,p,debug=False)
	alpha = sqnm((y1-y2)*d,1,p,debug=False)

	dSq = find_inverse((x1-x2)**2,p,debug=False)
	alphaSq = sqnm(((y1-y2)**2)*dSq,1,p,debug=False)

	x3 = sqnm(alphaSq-x2-x1,1,p,debug=False)
	y3 = sqnm(alpha*(x1-x3)-y1,1,p,debug=False)

	return x3,y3


def addP(P,Q,a,p):
	R=()
	AN=(P[1]-Q[1]);
	AD=(P[0]-Q[0])%p;
	if AD==0:
		return 'O'
		
	AN= (ModularInverse.findModularInverse(p, AD) * AN) % p;
	XN=((AN**2)-P[0]-Q[0])%p;
	R=(XN,0);
	YN=((AN*(P[0]-R[0]))-P[1])%p;
	R=(R[0],YN);
	return R;

def main(argv):
	P=(int(argv[1]),int(argv[2]));
	a=int(argv[3]);
	p=int(argv[4]);
  	R=doubleP(P,a,p);		#xp,yp,a,p

  	print R;
  	Result=addP(P,R,a,p);
  	print Result;


print doubleP((0,3),2,23)
print addP((0,3),(22,12),2,23)
print find2P((0,3),2,23)
print addPQ((0,3),(18,14),2,23)
print addPQ((0,3),(8,10),2,23)
#findxP(11,(8,10),2,23)

# if __name__ == '__main__':
# 	main(sys.argv);
