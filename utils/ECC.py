from utils import find_inverse, sqnm
import numpy


def find2P(P, a, p):
    """
    Find 2P
    :param P:
    :param a:
    :param p:
    :return:
    """
    x, y = P
    d = find_inverse(2 * y, p, debug=False)
    alpha = sqnm(((3 * ((x) ** 2)) + a) * d, 1, p, debug=False)

    dSq = find_inverse((2 * y) ** 2, p, debug=False)
    alphaSq = sqnm(((3 * ((x) ** 2)) + a) ** 2 * dSq, 1, p, debug=False)

    X2p = (alphaSq - 2 * x) % p
    Y2p = (alpha * (x - X2p) - y) % p
    return X2p, Y2p


def findxP(x, P, a, p):
    """
    Find xP eg: 11P
    :param x:
    :param P:
    :param a:
    :param p:
    :return:
    """
    Q = find2P(P, a, p)

    for i in range(x):
        Q = addPQ(P, Q, a, p)
        print Q,

def order(P,a,p):
    ord = 1
    Q = find2P(P,a,p)
    while Q!=0:
        ord+=1
        Q = addPQ(P,Q,a,p)
    return ord+1

def generateP(P,a,p):
    """
    Generate xP until Order of P or until it becomes inifinity
    :param P:
    :param a:
    :param p:
    :param table:
    :return:
    """
    res = []
    Q = find2P(P, a, p)
    res.append(P)
    res.append(Q)

    while Q!=0:
        Q = addPQ(P, Q, a, p)
        res.append(Q)
    res.append(len(res))
    return res


def addPQ(P, Q, a, p):
    """
    Add two EC points P and Q
    :param P:
    :param Q:
    :param a:
    :param p:
    :return:
    """
    x1, y1 = P
    x2, y2 = Q

    try:
        chk = sqnm((x1 - x2), 1, p, debug=False)
    except Exception, e:
        return 0
    if chk == 0:
        return 0
    d = find_inverse(chk, p, debug=False)
    alpha = sqnm((y1 - y2) * d, 1, p, debug=False)

    dSq = find_inverse((x1 - x2) ** 2, p, debug=False)
    alphaSq = sqnm(((y1 - y2) ** 2) * dSq, 1, p, debug=False)

    x3 = sqnm(alphaSq - x2 - x1, 1, p, debug=False)
    y3 = sqnm(alpha * (x1 - x3) - y1, 1, p, debug=False)

    return x3, y3


def findRoot(x, a, b, p):
    y = sqnm(((x) ** 3 + a * x + b), 1, p, debug=False)
    r1 = sqnm(y ** ((p + 1) / 4), 1, p, debug=False)
    r2 = p - r1
    if sqnm(r1 ** 2, 1, p, debug=False) == y and sqnm(r2 ** 2, 1, p, debug=False) == y:
        return r1, r2
    return None

def findGenerators(a,b,p):
    arr=[]
    for x in range(p):
        roots = findRoot(x,a,b,p)
        if roots:
            r1,r2=roots
            #print x,roots
            if r1==0 or r2==0: continue
            arr.append(generateP((x,r1),a,p))
            arr.append(generateP((x,r2),a,p))
    return arr


def findPosition(P,G):
    idx=None
    for i,x in enumerate(G):
        if P==x[0]:
            idx=i
            break
    return idx

def mul(n,P,a,p,G):
    idx = findPosition(P,G)
    l = len(G[idx])-2
    if n<l:
        return G[idx][n-1]
    else:
        Q = G[idx][l-1]
        n=n-l
        while n>0:
            if n<=l:
                if Q==G[idx][n-1]:
                    Q = find2P(Q,a,p)
                else:
                    Q = addPQ(Q,G[idx][n-1],a,p)
                return Q
            else:
                n=n-l
                if Q==G[idx][l-1]:
                    Q = find2P(Q,a,p)
                else:
                    Q = addPQ(Q,G[idx][l-1],a,p)

#G=findGenerators(7,15,3571)
# print mul(8,(2,7),6,11,G)
# print mul(6,(2,7),6,11,G)
# print mul(6,(3,5),6,11,G)
#print mul(150,(16,3096),15,3571,G)
# print find2P((8,13),2,23)
#print order((8,10),2,23)
