"""
__author__      = "Deepakshankar"

"""


def sqnm(u,m,p,debug=True):
    """
    SQUARE AND MULTIPLY
    :param u: base
    :param m: power
    :param p: mod
    :return: A
    """

    from texttable import Texttable
    def calc_m(m,b):
        return (m-b)/2

    def calc_b(m):
        return m%2

    def calc_u(u,p):
        return (u**2)%p

    def calc_Au(A,u):
        return A*u

    def calc_A(n,p):
        return n%p
    b=0
    A=1
    powers_u = 1
    powers_A = 1
    str_powersA = 1
    table = Texttable()
    table.set_cols_align(['c','c','c','c','c','c','c'])
    table.add_row(['m','b','u','Eval u','A','Eval A','Comments'])
    #stop=u**m
    #au= 1
    A = 1
    while 1:
        b= calc_b(m)
        if b!=0:
            if powers_A==1:
                powers_A=powers_u
            else:
                powers_A = powers_A+powers_u
            str_powersA = 'u^'+str(powers_A)
            au= calc_Au(A,u)
            A = calc_A(au,p)
            comment = "Multiply"
        else:
            comment = "Skip"
        table.add_row([m,b,'u^'+str(powers_u),u,str_powersA,A,comment])
        #print m,"\t",b,"\t",u,"\t",A
        if m==1 and b==1:
            break
        m = calc_m(m,b)
        u = calc_u(u,p)
        powers_u=powers_u*2
    if debug:
        print table.draw()
    return A
#
# print sqnm(7,27,30)
# +----+---+------+--------+------+--------+----------+
# | m  | b |  u   | Eval u |  A   | Eval A | Comments |
# +----+---+------+--------+------+--------+----------+
# | 27 | 1 | u^1  |   7    | u^1  |   7    | Multiply |
# +----+---+------+--------+------+--------+----------+
# | 13 | 1 | u^2  |   19   | u^2  |   13   | Multiply |
# +----+---+------+--------+------+--------+----------+
# | 6  | 0 | u^4  |   1    | u^2  |   13   |   Skip   |
# +----+---+------+--------+------+--------+----------+
# | 3  | 1 | u^8  |   1    | u^10 |   13   | Multiply |
# +----+---+------+--------+------+--------+----------+
# | 1  | 1 | u^16 |   1    | u^26 |   13   | Multiply |
# +----+---+------+--------+------+--------+----------+
# 13