"""
__author__      = "Deepakshankar"

"""

from utils import sqnm

dict = {'a':'00','b':'01','c':'02','d':'03','e':'04','f':'05','g':'06','h':'07',
            'i':'08','j':'09','k':'10','l':'11','m':'12','n':'13','o':'14','p':'15',
            'q':'16','r':'17','s':'18','t':'19','u':'20','v':'21','w':'22','x':'23',
            'y':'24','z':'25',' ':'32'}

def convert_data_to_numerical(text):
    """

    :param text:
    :return:
    """
    numerical_data = []
    for x in text:
        numerical_data.append(dict[x.lower()])
    return '|'.join(numerical_data)

def compress_data(data):
    """

    :param data:
    :return:
    """
    data = str(data)
    data_lst = data.split("|")
    data_lst = data_lst[::-1]
    compressed = []
    for x in range(len(data_lst)):
        compressed.append(int(data_lst[x])*(26**x))
    compressed = compressed[::-1]
    return sum(compressed)

def decode(compressed):
    """

    :param compressed:
    :return:
    """
    res = {v : k for k, v in dict.iteritems()}
    compressed = int(compressed)
    data = []
    while compressed>26:
        val=sqnm(compressed,1,26,debug=False)
        compressed=(compressed-val)/26
        if len(str(val))==1:
            data.append('0'+str(val))
        else:
            data.append(str(val))
    if len(str(compressed))==1:
        data.append('0'+str(compressed))
    else:
        data.append(str(compressed))
    data = data[::-1]
    s=''
    for x in data:
        s+=res[x]
    return s

def compressByblocks(text,debug=True):
    compressed_data = str(compress_data(convert_data_to_numerical(text)))
    compressed_data = compressed_data if len(compressed_data) % 2 == 0 else '0' + compressed_data
    if debug:
        print '\nData in numerical format => ',convert_data_to_numerical(text)
    cdata = []
    pos = 0
    for x in range(len(compressed_data) / 2):
        cdata.append(compressed_data[pos:pos + 2])
        pos += 2
    return cdata

#compress_data('13|09|08|19')
#print decode(234799)
#print convert_data_to_numerical('crypto')
#print compress_data(convert_data_to_numerical('crypto'))
