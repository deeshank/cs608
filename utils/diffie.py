"""
__author__      = "Deepakshankar"

"""

from utils import sqnm


def calculate_shared_key(g, a, b, p, debug=True):
    alice = sqnm(g, a, p, debug=False)
    if debug:
        print "Alice Sends key ==> ", alice
    bob = sqnm(g, b, p, debug=False)
    if debug:
        print "Bob Sends Key   ==> ", bob
    key1 = sqnm(alice, b, p, debug=False)
    key2 = sqnm(bob, a, p, debug=False)
    if debug:
        if key1 == key2:
            print "Shared Key      ==> ", key1
        else:
            print "Shared Key      ==> Key Mismatch"
    return key1 if key1 == key2 else "Key Mismatch"
