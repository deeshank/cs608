"""
__author__      = "Deepakshankar"

"""

import random
from utils import *


def elgammal(text, a, b, p):
    k = 5#random.randint(2, p - 1)
    print 'Random Key K => ', k

    g = find_generators(find_factors(p - 1), p)
    print 'Generator used => ', g

    compressed_data = compressByblocks(text)
    print '\nCompressed data => ', ''.join(compressed_data)
    print '\nCompressed data in block size 2 => ', compressed_data

    Pb = sqnm(g, b, p, debug=False)
    print '\nPb = g^b mod p => ', Pb

    M = sqnm(Pb, k, p, debug=False)
    print '\nM = Pb^k mod p => ', M

    C = []
    for x in compressed_data:
        C.append(sqnm(int(x) * M, 1, p, debug=False))

    print '\nC = (mM) mod p => ', C
    print '\nCipher => ', ''.join(map(str, C))

    H = sqnm(g, k, p, debug=False)
    print '\nH = g^k mod p =>', H

    q = p - 1 - b
    print '\n q => ',q
    R = sqnm(H, q, p, debug=False)
    print '\nR = H^q mod p => ', R

    D = []
    for x in C:
        tmp = str(sqnm(x * R, 1, p, debug=False))
        if len(tmp)==1:
            D.append('0'+tmp)
        else:
            D.append(tmp)

    print D
    D = ''.join(D)
    D = D if len(D) % 2 == 0 else '0' + D
    print '\nD => ', D
    print '\ndecoded data => ', decode(D)


def elgammal_dig_sig(M, p):
    # GCD anonymous function
    gcd = lambda m, n: m if not n else gcd(n, m % n)

    g = find_generators(find_factors(p - 1), p)
    print '\nGenerator g used => ', g

    r = random.randint(0, p - 1)
    print '\nRandom r (0 .. p-1) => ', r

    K = sqnm(g, r, p, debug=False)
    print '\nK = g^r mod p => ', K

    R = 0
    for x in range(2, p):
        if gcd(x, p - 1) == 1:
            R = x
            break

    print '\nR chosen (1 .. p-1) & gcd(R,p-1)=1 => ', R

    X = sqnm(g, R, p, debug=False)
    print '\nX = r^R mod p => ', X

    Y = findX(R, M - r * X, p - 1, debug=False)
    print '\nY = (M-rX) R^-1 mod (p-1) => ', Y
    print '\n(%d,%d) is the signature of M' % (X, Y)

    A1 = sqnm(sqnm(K, X, p, debug=False) * sqnm(X, Y, p, debug=False), 1, p, debug=False)
    print '\nReceiver B computes A = (K^X)*(X*Y) mod p => ', A1

    A2 = sqnm(g, M, p, debug=False)
    print '\nB accepts M if and only if A = g^M mod p => ', A2

    if A1 == A2:
        print 'Message Accepted'
    else:
        print 'Invalid Signature'

# elgammal_dig_sig(11,746951)
