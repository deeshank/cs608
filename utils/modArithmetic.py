"""
__author__      = "Deepakshankar"

"""
from Square_Multiply import sqnm


def simplify(a, b, p, debug=True):
    """
    Reduce or Simplify the powers
    3^118 mod 23 ==> 3^19 mod 23

    :param a: base
    :param b: power
    :param p: mod
    :return:
    """

    if a < 0:
        a += (p * ((abs(a) / p) + 1))
    if a > p:
        a = a % p
    if b > p:
        b = b % (p - 1)
    return a, b, p


def find_inverse(n, p, debug=True):
    """
    F.I.S.H Extended Euclid Algorithm
    :param n:
    :param p:
    :param debug:
    :return:
    """
    from texttable import Texttable
    table = Texttable()
    P = p
    q = ['q']
    r = [p, n]
    Q = p / n
    R = p % n
    q.append(Q)
    r.append(R)
    if R==0:
        return 1
    while R != 1:
        p = n
        n = R
        Q = p / n
        R = p % n
        q.append(Q)
        r.append(R)
    q.append("-")
    table.add_row(r)
    table.add_row(q)
    add = [0, 1]
    s = 1
    for x in range(len(q) - 2, 0, -1):
        add.append(add[s] * q[x] + add[s - 1])
        s += 1
    add = add[::-1]
    table.add_row(add)
    if debug:
        print table.draw()
    if (len(r) - 1) % 2 == 0:
        return P - add[0]
    else:
        return add[0]


# print find_inverse(16,21)
# print find_inverse(1973,2004)

def findX(a, u, p, debug=False):
    """
    evaluate ax=u (mod p)
    :param a:
    :param u:
    :param p:
    :return:
    """
    if u < 0:
        u, _, p = simplify(u, 1, p)
    i = find_inverse(a, p, debug)
    if debug:
        print 'Inv: ', i
    return u * i % p


def isPrime(n):
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return True


def find_factors(n):
    return list(set([x for x in range(2, n + 1) if n % x == 0 and isPrime(x)]))


def find_generators(factors, p, stop=100):
    values = []
    for x in factors:
        values.append((p - 1) / x)

    for x in range(1, stop):
        z = sqnm(x, values[0], p, debug=False)
        if z != 1:
            cont = 0
            for y in range(1, len(values)):
                Z = sqnm(x, values[y], p, debug=False)
                if Z == 1:
                    cont = 1
                    break
            if cont == 1:
                continue
            else:
                return x