"""
__author__      = "Deepakshankar"

"""

from data_utils import *
from utils import *
from modArithmetic import *


def find_coprime(m):
    gcd = lambda m, n: m if not n else gcd(n, m % n)
    e = 0
    for x in range(2, 100):
        if gcd(x, m) == 1:
            e = x
            break
    return e


def RSA(text, p, q, r, s):
    m = p * q
    print '\nm = p*q => ',m

    phim = (p - 1) * (q - 1)
    print '\nphi(m) = (p-1)*(q-1) => ',phim

    e = find_coprime(phim)
    print '\ne which is co-prime to phi(m) => ',e

    d = find_inverse(e, phim, debug=False)
    print '\nd is modular inverse of e => ',d

    n = r * s
    print '\nn = r*s => ',n

    phin = (r - 1) * (s - 1)
    print '\nphi(n) = (r-1)*(s-1) => ',phin

    g = find_coprime(phin)
    print '\ng is co-prime of phi(n) => ',g

    h = find_inverse(g, phin, debug=False)
    print '\nh is modular inverse of g => ',h

    num = convert_data_to_numerical(text)
    print '\n',text,' is converted to numerical format => ',num
    compressed_data = compress_data(num)
    print '\ndata is compressed to => ',compressed_data

    x = sqnm(compressed_data, d, m, debug=False)
    print '\nx = a^d mod m => ',x

    y = sqnm(x, h, n, debug=False)
    print '\ny = x^h mod n => ',y

    print "\nA sending y to receiver B via internet"

    z = sqnm(y, g, n, debug=False)
    print '\nDecrypt:'
    print '\nz = y^g mod n => ',z

    u = sqnm(z, e, m, debug=False)
    print '\nVerify:'
    print '\nu = z^e mod m => ',u

    data = decode(u)
    print '\nDecoding the Compressed Message to String  => ',data


