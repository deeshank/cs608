import random
import numpy


def solve(EQ,points,xdata,ydata):
    lhs=[]
    rhs=[]
    for x in points:
        tmp_eqn = EQ.replace('x',str(xdata[x])).replace('y',str(ydata[x]))
        lhs.append(map(eval,tmp_eqn.split("=")[0].strip().split('+')))
        rhs.append(tmp_eqn.split("=")[1].strip())
    a=numpy.array(lhs)
    b=numpy.array(rhs)
    soln = numpy.linalg.lstsq(a,b)
    return soln[0]


def Xk(n,table=True):
    n=n+1
    data = {}
    for x in xrange(n):
        data[(2*x)-1]=x
        data[2*x]=-x
    if table:
        from texttable import Texttable
        table = Texttable()
        table.add_row([x for x in xrange(n)])
        table.add_row([data[x] for x in xrange(n)])
        return data,table
    return data

def Yk(n,eqn,xdata,tbl, table=True):
    n=n+1
    ydata = {}
    for x in xrange(n):
        teqn = eqn.replace('x',str(xdata[x]))
        ydata[x]=eval(teqn)
    if table:
        tbl.add_row([ydata[x] for x in xrange(n)])
        return ydata,tbl


def secretSharing(eqn):
    n=3
    m=4
    s=m+n
    xdata,tbl=Xk(s)
    ydata,tbl=Yk(s,eqn,xdata,tbl)
    print tbl.draw()
    pts = random.sample(range(n+1),3)

    EQ="((x)**2)+(x)+1=y"
    soln = solve(EQ,pts,xdata,ydata)
    print "Solving Linear Equation => ",soln

    P={x:(xdata[x],ydata[x]) for x in range(s+1) }
    print P

    k=3
    i=4

    CEO=[P[1],P[2],P[3]]
    PRESIDENT=[P[1],P[2],P[3]]
    VP=[P[0],P[k]]
    MANAGER=[P[n+i]]

    print CEO
    print PRESIDENT
    print VP
    print MANAGER


    print "A,B,C => ",solve(EQ,[0,k,n+i],xdata,ydata)

    print "A,B,C => ",solve(EQ,[0,3,4],xdata,ydata)


# eqn = '3*(x)**2+8*(x)+5'
# secretSharing(eqn)