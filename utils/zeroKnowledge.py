from utils import find_inverse

p = 7
q = 11

N = p*q

priv1 = 9
priv2 = 10

pub1 = find_inverse(priv1**2,N,debug=False)
pub2 = find_inverse(priv2**2,N,debug=False)

print pub1,pub2

random_nos = [19,24,51]

a,b,c = random_nos
A,B,C = [x**2%N for x in random_nos]

matrix = [[0,1],[1,0],[1,1]]

Ka,Kb,Kc = matrix

M,P,Q = [x*priv1**matrix[i][0]*priv2**matrix[i][1]%N for i,x in enumerate(random_nos)]

print "M := [a(Priv1^0 Priv2^1)] mod N => %d x %d^%d  x %d^%d mod %d = %d"%(a,priv1,Ka[0],priv2, Ka[1],N,M)
print "P := [b(Priv1^1 Priv2^0)] mod N => %d x %d^%d  x %d^%d mod %d = %d"%(b,priv1,Kb[0],priv2, Kb[1],N,P)
print "Q := [c(Priv1^1 Priv2^1)] mod N => %d x %d^%d  x %d^%d mod %d = %d"%(c,priv1,Kc[0],priv2, Kc[1],N,Q)
print ''

A1,B1,C1 = [x**2*pub1**matrix[i][0]*pub2**matrix[i][1]%N for i,x in enumerate([M,P,Q])]


print "A := [a^2(Pub1^0 Pub2^1)] mod N => %d^2 x %d^%d  x %d^%d mod %d = %d"%(M,pub1,Ka[0],pub2, Ka[1],N,A1)
print "B := [b^2(Pub1^1 Pub2^0)] mod N => %d^2 x %d^%d  x %d^%d mod %d = %d"%(P,pub1,Kb[0],pub2, Kb[1],N,B1)
print "C := [c^2(Pub1^1 Pub2^1)] mod N => %d^2 x %d^%d  x %d^%d mod %d = %d"%(Q,pub1,Kc[0],pub2, Kc[1],N,C1)
print ''